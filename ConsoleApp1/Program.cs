﻿using System.Diagnostics;

namespace ConsoleApp1
{
    public class Program
    {
        static void Main(string[] args)
        {
            var countMass = new int[] { 100000, 1000000, 10000000 };
            foreach (var count in countMass)
            {
                Console.WriteLine($"Массив на {count} чисел");
                var mass = new int[count];
                
                for (int i = 0; i < count; i++)
                    mass[i] = Random.Shared.Next(-1000, 1000);

                var stopwatch = new Stopwatch();
                stopwatch.Start();
                var sum1 = 0;
                for (int i = 0; i < count; i++)
                    sum1 += mass[i];
                stopwatch.Stop();
                Console.WriteLine($"Стандартное вычисление через цикл for: {stopwatch.ElapsedMilliseconds}мс");

                stopwatch.Restart();
                var sum2 = 0;
                Parallel.ForEach(mass.ToList(), new ParallelOptions()
                {
                    MaxDegreeOfParallelism = 3
                }, val =>
                {
                    Interlocked.Add(ref sum2, val);
                });
                stopwatch.Stop();
                Console.WriteLine($"Параллельное вычисление list: {stopwatch.ElapsedMilliseconds}мс");

                stopwatch.Restart();
                var sum3 = mass.AsParallel().Sum();
                stopwatch.Stop();
                Console.WriteLine($"Параллельное вычисление linq: {stopwatch.ElapsedMilliseconds}мс\n");
            }

            Console.ReadLine();
        }

    }
}